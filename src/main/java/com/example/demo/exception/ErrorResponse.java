package com.example.demo.exception;

import lombok.Builder;
import lombok.Data;

/**
 * gradle-demo
 * Elkhan
 * 10.02.2024 18:54
 */
@Data
@Builder
public class ErrorResponse {
    private String message;
    private int status;
}
