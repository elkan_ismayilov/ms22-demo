package com.example.demo.exception;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * gradle-demo
 * Elkhan
 * 10.02.2024 18:55
 */
@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
    @ExceptionHandler(StudentServiceException.class)
    public ResponseEntity<ErrorResponse> handleStudentServiceException(StudentServiceException ex) {
        ErrorResponse errorResponse = ErrorResponse.builder()
                .message(ex.getMessage())
                .status(400)
                .build();
        return ResponseEntity.status(400).body(errorResponse);
    }
}
