package com.example.demo.exception;

/**
 * gradle-demo
 * Elkhan
 * 10.02.2024 18:54
 */
public class StudentServiceException extends RuntimeException {
    public StudentServiceException(String message) {
        super(message);
    }
}
