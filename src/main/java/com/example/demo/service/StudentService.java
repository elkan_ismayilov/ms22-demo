package com.example.demo.service;

import com.example.demo.dto.StudentRequest;
import com.example.demo.dto.StudentResponse;

import java.util.List;

/**
 * gradle-demo
 * Elkhan
 * 28.01.2024 09:26
 */
public interface StudentService {
    StudentResponse addStudent(StudentRequest studentRequest);
    List<StudentResponse> getStudents();
    StudentResponse getStudent(Long id);
    StudentResponse updateStudent(Long id, StudentRequest studentRequest);
    void deleteStudent(Long id);
}
