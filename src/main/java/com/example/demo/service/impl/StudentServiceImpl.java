package com.example.demo.service.impl;

import com.example.demo.dto.StudentRequest;
import com.example.demo.dto.StudentResponse;
import com.example.demo.entity.Student;
import com.example.demo.exception.StudentServiceException;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * gradle-demo
 * Elkhan
 * 28.01.2024 09:27
 */
@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {
    private final StudentRepository studentRepository;

    @Override
    public StudentResponse addStudent(StudentRequest studentRequest) {
        if (studentRequest.getName() == null || studentRequest.getName().isEmpty()) {
            throw new StudentServiceException("Name must be filled");
        }
        if (studentRequest.getSurname() == null || studentRequest.getSurname().isEmpty()){
            throw new StudentServiceException("Surname must be filled");
        }
        if (studentRequest.getAge() == null || studentRequest.getAge() <= 0){
            throw new StudentServiceException("Age must be greater than 0");
        }
        Student student = Student.builder()
                .name(studentRequest.getName())
                .surname(studentRequest.getSurname())
                .age(studentRequest.getAge())
                .build();
        Student savedStudent = studentRepository.save(student);
        return StudentResponse.builder()
                .id(savedStudent.getId())
                .name(savedStudent.getName())
                .surname(savedStudent.getSurname())
                .age(savedStudent.getAge())
                .build();
    }

    @Override
    public List<StudentResponse> getStudents() {
        return studentRepository.findAll().stream()
                .map(this::convertStudentToStudentResponse)
                .toList();
    }

    @Override
    public StudentResponse getStudent(Long id) {
        Student student = studentRepository.findById(id).orElseThrow(() -> new StudentServiceException("Student not found"));
        return StudentResponse.builder()
                .id(student.getId())
                .name(student.getName())
                .surname(student.getSurname())
                .age(student.getAge())
                .build();
    }

    @Override
    public StudentResponse updateStudent(Long id, StudentRequest studentRequest) {
        StudentResponse studentResponse = getStudent(id);
        if (studentRequest.getName() != null && !studentRequest.getName().isEmpty()) {
            studentResponse.setName(studentRequest.getName());
        }
        if (studentRequest.getSurname() != null && !studentRequest.getSurname().isEmpty()) {
            studentResponse.setSurname(studentRequest.getSurname());
        }
        if (studentRequest.getAge() != null && studentRequest.getAge() > 0) {
            studentResponse.setAge(studentRequest.getAge());
        }
        Student updateStudent = convertStudentResponseToStudent(studentResponse);
        studentRepository.save(updateStudent);
        return studentResponse;
    }

    @Override
    public void deleteStudent(Long id) {
        StudentResponse studentResponse = getStudent(id);
        Student student = convertStudentResponseToStudent(studentResponse);
        studentRepository.delete(student);
    }

    private Student convertStudentResponseToStudent(StudentResponse studentResponse) {
        return Student.builder()
                .id(studentResponse.getId())
                .name(studentResponse.getName())
                .surname(studentResponse.getSurname())
                .age(studentResponse.getAge())
                .build();
    }

    private StudentResponse convertStudentToStudentResponse(Student student) {
        return StudentResponse.builder()
                .id(student.getId())
                .name(student.getName())
                .surname(student.getSurname())
                .age(student.getAge())
                .build();
    }
}
