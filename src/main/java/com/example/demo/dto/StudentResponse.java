package com.example.demo.dto;

import lombok.Builder;
import lombok.Data;

/**
 * gradle-demo
 * Elkhan
 * 28.01.2024 09:28
 */
@Data
@Builder
public class StudentResponse {
    private Long id;
    private String name;
    private String surname;
    private Integer age;
}
