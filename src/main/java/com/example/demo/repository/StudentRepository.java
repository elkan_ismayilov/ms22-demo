package com.example.demo.repository;

import com.example.demo.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * gradle-demo
 * Elkhan
 * 28.01.2024 09:46
 */
public interface StudentRepository extends JpaRepository<Student, Long> {
}
