**To run studentapp in your local environment just clone the project and run the following command 
in the root directory of the project:**

`docker-compose up`

1. Application endpoints are available at http://localhost:8080
2. Here is the list of endpoints:
    - GET http://localhost:8080/api/v1/student - get all students
    - GET http://localhost:8080/api/v1/student/{id} - get student by id
    - POST http://localhost:8080/api/v1/student - create student
    - PUT http://localhost:8080/api/v1/student/{id} - update student by id
    - DELETE http://localhost:8080/api/v1/student/{id} - delete student by id